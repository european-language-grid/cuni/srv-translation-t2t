#!/bin/sh
dir=$(dirname $0)
tensorflow_model_server \
    --port=8002 \
    --model_config_file=/translation/models/model.config.$1 \
    --enable_batching=true \
    --batching_parameters_file=/translation/models/batching.config &
sleep 15  # Wait for the server to start
python3 /translation/t2t-serving-adapter.py \
    --models $1 \
    --port=8080 \
    --max_request_size=1024000 \
    --tfserving_host=localhost \
    --tfserving_port=8002
