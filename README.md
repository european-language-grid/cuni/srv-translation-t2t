# TFServing Translation service Docker image for ELG

This repository is a `docker` image builder for TF Serving translation service for ELG.

The content of this repository is available under Apache 2.0 license.
The translation models are available under CC BY-NC-SA.

The REST server is running on port 8080 by default.

To translate by a model for a _language-pair_, simply send HTTP POST to /translation/_language-pair_ (e.g., /translation/en-cs)
