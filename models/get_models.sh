#!/bin/sh

set -e

dir=$(dirname $0)

# en-cs cs-en en-fr fr-en en-hi
for model in $@; do
    url=https://ufallab.ms.mff.cuni.cz/~varis/elg_models/t2t-model.$model.zip
    fname=${url##*/}
    curl $url -o $dir/$fname
    unzip -d $dir $dir/$fname
    mv $dir/${fname%%.zip}/vocab* $dir/../data_dir
    rm $dir/$fname
done
